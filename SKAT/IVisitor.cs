﻿using System;

namespace SKAT
{
    public interface IVisitor
    {
        DateTime StartDate { get; }
        DateTime EndDate { get; }
        int MfcId { get; }
        int Federal { get; }
        int Marginal { get; }
        int Municipal { get; }

        string ToString();
    }
}
