﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace SKAT
{
    public partial class MainForm : Form
    {
        private SkatApi api;
        private Database db;

        public MainForm()
        {
            InitializeComponent();
        }

        private void OnResponse(IVisitor[] visitors)
        {
            Thread thread = new Thread(() => UpdateData(visitors));
            thread.Name = "DataUpdater";
            thread.IsBackground = true;
            thread.Start();
        }

        private void UpdateData(IVisitor[] visitors)
        {
            bool isRequireUpdate = false;

            foreach (IVisitor v in visitors)
            {
                var visitor = new SqlVisitor();
                visitor.StartDate = v.StartDate;
                visitor.EndDate = v.EndDate;
                visitor.MfcId = v.MfcId;
                visitor.Federal = v.Federal;
                visitor.Marginal = v.Marginal;
                visitor.Municipal = v.Municipal;

                if (db.Add(visitor))
                {
                    isRequireUpdate = true;
                }
            }

            if (isRequireUpdate)
            {
                LoadTable();
            }
        }

        private void LoadTable()
        {
            var vs = db.FindVisitors(new string[] { SqlVisitor.StartDateField, SqlVisitor.EndDateField });

            Invoke(new Action(() =>
            {
                dgvTable.Rows.Clear();

                foreach (var v in vs)
                {
                    dgvTable.Rows.Add(
                        v.Id.ToString(),
                        v.StartDate.ToString("dd.MM.yyyy"),
                        v.EndDate.ToString("dd.MM.yyyy"),
                        v.MfcId.ToString(),
                        v.Federal.ToString(),
                        v.Marginal.ToString(),
                        v.Municipal.ToString());
                }
            }));
        }

        private void bFetch_Click(object sender, EventArgs e)
        {
            DateTime date1 = dtpStart.Value;
            DateTime date2 = dtpEnd.Value;
            api.RequestVisitors(date1, date2, OnResponse);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            api = new SkatApi();
            db = new Database();

            if (!db.Connect())
            {
                MessageBox.Show("Database connection failed!");
            }
            else
            {
                Thread thread = new Thread(() => LoadTable());
                thread.Name = "TableLoader";
                thread.IsBackground = true;
                thread.Start();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Disconnect();
        }
    }
}
