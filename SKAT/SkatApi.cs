﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;

using SKAT.Service;

namespace SKAT
{
    public class SkatApi
    {
        public delegate void OnResponse(IVisitor[] values);

        public const int IdOdt = 112;

        private WebServiceClient client = new WebServiceClient();

        private bool isCanceled = false;

        /// <summary>
        /// Returns all visitors by the specified period in response-function.
        /// This method will run in the background thread.
        /// </summary>
        /// <param name="date1">Start date of a period</param>
        /// <param name="date2">End date of a period</param>
        /// <param name="response">Response listener</param>
        public void RequestVisitors(DateTime date1, DateTime date2, OnResponse response)
        {
            var thread = new Thread(() => Visitors(date1, date2, response));
            thread.Name = "VisitorsLoader";
            thread.IsBackground = true;
            thread.Start();
        }

        /// <summary>
        /// Cancels all pending requests. The class instance can't be used
        /// after calling this method.
        /// </summary>
        public void Destroy()
        {
            isCanceled = true;
        }
             
        private void Visitors(DateTime date1, DateTime date2, OnResponse response)
        {
            modelServicesNumber[] array;
            //TODO: It needs to read more about the generated WebServiceClient.
            //If it supports a multi-threading work then we can unlock the client
            //while make a request.
            lock (client)
            {
                if (isCanceled) //The request was canceled by the user: finish thread.
                {
                    return;
                }
                
                array = client.visitors(IdOdt, date1.ToString("dd.MM.yyyy"), date2.ToString("dd.MM.yyyy"));
            }

            var visitors = new IVisitor[array.Length];

            for (int i = 0; i < array.Length; ++i)
            {
                var num = array[i];
                DateTime date;

                if (!DateTime.TryParseExact(num.data, "dd'.'MM'.'yyyy", CultureInfo.InvariantCulture,
                           DateTimeStyles.None, out date))
                {
                    Debug.WriteLine("Date parising failed for '" + num.data + "'");
                }

                visitors[i] = new Visitor(date1, date2, num.mfcid, num.federal, num.marginal, num.municipal);
            }

            var handlerDelegate = new Action<IVisitor[]>(delegate (IVisitor[] values) {
                response(values);
            });
            handlerDelegate.Invoke(visitors);
        }

        public class Visitor : IVisitor
        {
            public Visitor(DateTime startDate, DateTime endDate, int mfcId, int federal, int marginal, int municipal)
            {
                StartDate = startDate;
                EndDate = endDate;
                MfcId = mfcId;
                Federal = federal;
                Marginal = marginal;
                Municipal = municipal;
            }

            public DateTime StartDate { get; }
            public DateTime EndDate { get; }
            public int MfcId { get; }
            public int Federal { get; }
            public int Marginal { get; }
            public int Municipal { get; }

            public override string ToString()
            {
                return "{" + StartDate.ToString("dd.MM.yyyy") + ", "
                    + EndDate.ToString("dd.MM.yyyy") + ", "
                    + MfcId + ", " + Federal + ", "
                    + Marginal + ", " + Municipal + "}";
            }
        }
    }
}
