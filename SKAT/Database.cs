﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;

namespace SKAT
{
    public class Database
    {
        private static readonly string DatabaseName;

        private static object Mutex = new object();

        private SqlConnection connection = new SqlConnection();

        static Database()
        {
            DatabaseName = Properties.Settings.Default.DatabaseName;
        }

        public bool Connect()
        {
            connection.ConnectionString = "Data Source=.\\SQLExpress;"
                + "User Instance=true;"
                + "Integrated Security=true;"
                + "AttachDbFilename=|DataDirectory|"
                + DatabaseName + ".mdf;";

            try
            {
                connection.Open();
                Create();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }

            return false;
        }

        public bool Disconnect()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }

            return false;
        }

        public bool Add(SqlVisitor visitor)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText =
                "INSERT INTO [dbo].[" + SqlVisitor.TableName + "] ("
                    + SqlVisitor.StartDateField + ", "
                    + SqlVisitor.EndDateField + ", "
                    + SqlVisitor.MfcIdField + ", "
                    + SqlVisitor.FederalField + ", "
                    + SqlVisitor.MarginalField + ", "
                    + SqlVisitor.MunicipalField
                    + ") VALUES (@start, @end, @mfc, @federal, @marginal, @municipal);";

            cmd.Parameters.AddWithValue("@start", visitor.StartDate.ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@end", visitor.EndDate.ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@mfc", visitor.MfcId);
            cmd.Parameters.AddWithValue("@federal", visitor.Federal);
            cmd.Parameters.AddWithValue("@marginal", visitor.Marginal);
            cmd.Parameters.AddWithValue("@municipal", visitor.Municipal);
            Debug.WriteLine(cmd.ToString());

            lock (Mutex)
            {
                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }

            return false;
        }

        public IList<SqlVisitor> FindVisitors(string[] orderByColumns)
        {
            var cmd = connection.CreateCommand();
            string sql = "SELECT * FROM [dbo].[" + SqlVisitor.TableName + "]";

            if (orderByColumns != null && orderByColumns.Length > 0)
            {
                sql += " ORDER BY ";

                for (int i = 0; i < (orderByColumns.Length - 1); ++i)
                {
                    sql += orderByColumns[i] + ", ";
                }

                sql += orderByColumns[orderByColumns.Length - 1] + ";";
            }
            else
            {
                sql += ";";
            }

            cmd.CommandText = sql;
            var visitors = new List<SqlVisitor>();

            lock (Mutex)
            {
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    SqlVisitor v = new SqlVisitor();
                    v.Id = int.Parse(reader[SqlVisitor.IdField].ToString());
                    v.StartDate = DateTime.Parse(reader[SqlVisitor.StartDateField].ToString());
                    v.EndDate = DateTime.Parse(reader[SqlVisitor.EndDateField].ToString());
                    v.MfcId = int.Parse(reader[SqlVisitor.MfcIdField].ToString());
                    v.Federal = int.Parse(reader[SqlVisitor.FederalField].ToString());
                    v.Marginal = int.Parse(reader[SqlVisitor.MarginalField].ToString());
                    v.Municipal = int.Parse(reader[SqlVisitor.MunicipalField].ToString());
                    visitors.Add(v);
                }

                reader.Close();
            }

            return visitors;
        }

        private void Create()
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].["
                + SqlVisitor.TableName + "]') AND type in (N'U')) "
                + " BEGIN "
                + SqlVisitor.CreationScript
                + " END ";

            lock (Mutex)
            {
                cmd.ExecuteNonQuery();
            }
        }
    }

    public class SqlVisitor : IVisitor
    {
        public const string TableName = "Visitors";

        public const string IdField = "Id";
        public const string StartDateField = "StartDate";
        public const string EndDateField = "EndDate";
        public const string MfcIdField = "MfcId";
        public const string FederalField = "Federal";
        public const string MarginalField = "Marginal";
        public const string MunicipalField = "Municipal";

        public const string CreationScript = "CREATE TABLE " + TableName + " ("
            + "Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL, "
            + "StartDate DATE NOT NULL, "
            + "EndDate DATE NOT NULL, "
            + "MfcId INT NOT NULL, "
            + "Federal INT NOT NULL, "
            + "Marginal INT NOT NULL, "
            + "Municipal INT NOT NULL, "
            + "CONSTRAINT uc_Period UNIQUE (StartDate, EndDate));";

        public int Id;

        public DateTime StartDate
        {
            get; set;
        }

        public DateTime EndDate
        {
            get; set;
        }

        public int MfcId
        {
            get; set;
        }

        public int Federal
        {
            get; set;
        }

        public int Marginal
        {
            get; set;
        }

        public int Municipal
        {
            get; set;
        }
    }
}
